# soal-shift-sisop-modul-2-D09-2022
## Anggota Kelompok
|NRP|Nama|Soal|
|---|----|---|
|5025201085|Renita Caithleen Davita|Soal 1|
|5025201257|Sastiara maulikh|Soal 2|
|5025201140|Ezekiel Mashal Wicaksono|Soal 3|
3. ## Soal 3
    * ### __soal3.c__
        Dalam soal diminta untuk membuat program untuk mendata apa saja hewan yang hilang. Untuk itu dalam Program ini terdiri dari banyak function.
        * > `void folder_darat()`
            ```
            void folder_darat(){
                char *argv[] = {"mkdir", "-p", "/home/ezekielmw/modul2/darat", NULL};
                execv("/bin/mkdir", argv);
            }
            ```
            Pada function ini saya menggunakan `func` builtin yang ada dalam linux . Untuk itu saya menggunakan `execv` dan memanggil `mkdir` . Untuk itu penulisannya dalam function ini mirip dengan meulis pada terminal linux dan dipisah menggunakan tanda *`"" , ""`* . Dalam `execv` ini diawali dengan memanggil `mkdir` dan menyatakan direktorinya yaitu `/home/ezekielmw/modul2/darat`. Command `-p` diguanakan untuk membuat folder. Kemudian diakhiri dengan `NULL`.
        * > `void folder_air()`
            ```
            void folder_air(){
                char *argv[] = {"mkdir", "-p", "/home/ezekielmw/modul2/air", NULL};
                execv("/bin/mkdir", argv);
            }
            ```
            Function ini sama dengan `void folder_darat` hanya bedanaya ini digunakan untuk membuat folder `air` . Untuk itu hanya ada perubahan dir folder menjadi `/home/ezekielmw/modul2/air`.
        * > `void folder_air()`
            ```
            void unzip_animal(){
                char *arg[] = {"unzip","/home/ezekielmw/modul2/animal.zip","-d","/home/ezekielmw/modul2", NULL};
                execv("/usr/bin/unzip",arg);
            }
            ```
            Dalam function ini memanggil `/bin/unzip` yang digunakan untuk unzip folder `animal.zip` , kemudian ditulis seperti di terminal yaitu command `unzip` kemudian dimasukan directory `.zip` nya dan memakai `-d` untuk membuat folder dan dimasukan directory tujuannya.
        * > `void kelompok()`
            ```
            void kelompok(){
                pid_t pid4;
                pid4 = fork();
                if (pid4 < 0) {
                    exit(EXIT_FAILURE); 
                }
                else if (pid4 == 0 ){
                    char *arg[]={"find","/home/ezekielmw/modul2/animal","-name","*darat*","-exec","mv","{}","/home/ezekielmw/modul2/darat",";",NULL};
                    execv("/usr/bin/find",arg);
                }
                else{
                    char *arg[]={"find","/home/ezekielmw/modul2/animal","-name","*air*","-exec","mv","{}","/home/ezekielmw/modul2/air",";",NULL};
                    execv("/usr/bin/find",arg);
                }
            }
            ```
            function `kelompok()` digunakan untuk mengelompokkan semua yang ada tulisan air ke dalam folder air dan semua tulisan darat ke folder darat. Karena akan ada 2 pengelompokkan makanya menggunakan `fork()` dalam fungsi ini. Parent akan mengeksekusi folder `darat` dan child mengeksekusi folder `air`. Saat ` (pid4<0)` maka akan exit dari fork , jika `(pid4 == 0)` maka akan memindahkan ke folder darat. Untuk memindahkan menggunakan func `find` kemudian memasukkan file dir , kemudian command `"-name","*darat*"` digunakan untuk mengambil file yang ada nama darat. Kemudian command `"-exec","mv","{}","/home/ezekielmw/modul2/darat"` digunakan untuk move dari folder animal ke folder darat. `-exec` menyatakan execute dan `mv` untuk move. Untuk folder air juga sama seperti darat , hanya beda directory dan `*air*`
        * > `void delete_frog()`
            ```
            void delete_frog (){
                char *arg[]={"rm","-r","/home/ezekielmw/modul2/animal/frog.jpg",NULL};
                execv("/usr/bin/rm",arg);
            }
            ```
            menggunakan command `rm` digunakan untuk mendelete file `frog.jpg`, kemudian command `-r` untuk mendelete file.
        * > `void delete_bird()`
            ```
            void delete_bird (){
                char *argv[]={"find","/home/ezekielmw/modul2/darat","-name","*bird*","-exec","rm","-rf","{}","\;",NULL};
                execv("/bin/find",argv);
            }
            ```
            pada function ini kurang lebih mirip dengan delete frog , hanya saja pada command ini nmenggunakan `find` dan juga menggunakan `"-name","*bird*"` yang artinya mencari file dengan unsur bird kemudian didelete.
        * > `void uid()`
            ```
            DIR *dp;
            struct dirent *ep;
            char filename[300], forstat[300];
            FILE *fptr;
            struct stat fs;
    
            fptr = fopen("air/list.txt", "w");

            dp = opendir("air");

            if (dp != NULL)
            {
                while ((ep = readdir (dp))) {
                    if(strcmp(ep->d_name, ".") == 0 || strcmp(ep->d_name, "..") == 0 || strcmp(ep->d_name, "list.txt") == 0) continue;
                    sprintf(forstat, "air/%s", ep->d_name);
                    stat(forstat, &fs);
                    sprintf(filename, "%s_", getpwuid(fs.st_uid)->pw_name);
                    if( fs.st_mode & S_IRUSR ) strcat(filename, "r");
                    if( fs.st_mode & S_IWUSR ) strcat(filename, "w");
                    if( fs.st_mode & S_IXUSR ) strcat(filename, "x");
                    strcat(filename, "_");
                    strcat(filename, ep->d_name);
                    fprintf(fptr, "%s\n", filename);
                }
                (void) closedir (dp);
            } else perror ("Couldn't open the directory");
            fclose(fptr);
            ```
            Fungsi yang digunakan untuk mengisi list.txt

        * > screenshot result
        
            https://drive.google.com/drive/folders/1h2LgtIlaV5qJ78YdPbOzB3EEQh30EJlj?usp=sharing
