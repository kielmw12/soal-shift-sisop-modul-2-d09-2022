#include <stdio.h>
#include <stdlib.h>
#include <pwd.h>
#include <grp.h>
#include <sys/stat.h>
#include <dirent.h>
#include <stddef.h>

//by Ezekiel Mashal W (5025201140)
void folder_darat(){
    char *argv[] = {"mkdir", "-p", "/home/ezekielmw/modul2/darat", NULL};
    execv("/bin/mkdir", argv);
}
void folder_air(){
    char *argv[] = {"mkdir", "-p", "/home/ezekielmw/modul2/air", NULL};
    execv("/bin/mkdir", argv);
}
void unzip_animal(){
    char *arg[] = {"unzip","/home/ezekielmw/modul2/animal.zip","-d","/home/ezekielmw/modul2", NULL};
    execv("/usr/bin/unzip",arg);
}
void kelompok(){
    pid_t pid4;
    pid4 = fork();
    if (pid4 < 0) {
        exit(EXIT_FAILURE); 
    }
    else if (pid4 == 0 ){
        char *arg[]={"find","/home/ezekielmw/modul2/animal","-name","*darat*","-exec","mv","{}","/home/ezekielmw/modul2/darat",";",NULL};
        execv("/usr/bin/find",arg);
    }
    else{
        char *arg[]={"find","/home/ezekielmw/modul2/animal","-name","*air*","-exec","mv","{}","/home/ezekielmw/modul2/air",";",NULL};
        execv("/usr/bin/find",arg);
    }

}
void delete_frog (){
    char *arg[]={"rm","-r","/home/ezekielmw/modul2/animal/frog.jpg",NULL};
    execv("/usr/bin/rm",arg);
}
void delete_bird (){
    char *argv[]={"find","/home/ezekielmw/modul2/darat","-name","*bird*","-exec","rm","-rf","{}","\;",NULL};
    execv("/bin/find",argv);
}
void uid(){
    DIR *dp;
    struct dirent *ep;
    char filename[300], forstat[300];
    FILE *fptr;
    struct stat fs;
    
    fptr = fopen("air/list.txt", "w");

    dp = opendir("air");

    if (dp != NULL)
    {
        while ((ep = readdir (dp))) {
            if(strcmp(ep->d_name, ".") == 0 || strcmp(ep->d_name, "..") == 0 || strcmp(ep->d_name, "list.txt") == 0) continue;
            sprintf(forstat, "air/%s", ep->d_name);
            stat(forstat, &fs);
            sprintf(filename, "%s_", getpwuid(fs.st_uid)->pw_name);
            if( fs.st_mode & S_IRUSR ) strcat(filename, "r");
            if( fs.st_mode & S_IWUSR ) strcat(filename, "w");
            if( fs.st_mode & S_IXUSR ) strcat(filename, "x");
            strcat(filename, "_");
            strcat(filename, ep->d_name);
            fprintf(fptr, "%s\n", filename);
        }
        (void) closedir (dp);
    } else perror ("Couldn't open the directory");
    
    fclose(fptr);
}
int main(){
    pid_t pid1 ;
    int status;
    pid1 = fork();
    if (pid1 < 0) {
        exit(EXIT_FAILURE); 
    }
    if (pid1 == 0) {
        folder_darat();
    }
    else {
        while ((wait(&status))>0);
        sleep(3);
        pid_t pid2;
        pid2 = fork();
        if (pid2 == 0){
            folder_air();
        }
        else if (pid2 > 0){
            while ((wait(&status))>0);
            pid_t pid3;
            pid3 = fork();
            if (pid3 == 0){
                unzip_animal();
            }
            else if (pid3 > 0){
                while ((wait(&status))>0);
                pid_t pid5;
                pid5 = fork();
                if (pid5==0){
                    kelompok();
                }
                else if (pid5 >0){
                    pid_t pid6;
                    pid6 = fork();
                    if (pid6==0){
                        delete_frog();
                    }
                    else if (pid6 > 0){
                        pid_t pid7;
                        pid7 = fork();
                        if (pid7 == 0){
                            delete_bird();
                        }
                        else if (pid7 >0){
                            uid();
                        }
                    }
                }
            }
        }
    }
}
